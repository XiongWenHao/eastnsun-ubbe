import React, { Component } from 'react';
import { Form, Input, Button, Alert } from 'antd';
import './Forget.css';
import axios from 'axios';

const FormItem = Form.Item;

class Forget extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                this.closeProgress(true);
                const forgetEmail = values.email;
               console.log(forgetEmail);

               this.props.history.push({
                   pathname:'./sent',
                   email:forgetEmail
               })
            }
        });
    }

    closeProgress = (e) => {
        this.setState({
            loading: e
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit}>
                <div className="forgetBody">
                    <div className="forgetContainer forgetBottom">
                        <div className="forgetTitle forgetTop forgetBottom">Forgot password?</div>

                        <div className="forgetHint forgetBottom">Enter your email and reset your password</div>
                        <FormItem>
                            {getFieldDecorator('email', {
                                rules: [{
                                    type: 'email', message: 'The input is not valid E-mail!',
                                }, {
                                    required: true, message: 'Please input your E-mail!',
                                }],
                            })(
                                <Input size="large" style={{ width: 260 }} placeholder="Enter Email" />
                            )}
                        </FormItem>
                    
                        <Button className="forgetButton forgetBottom" loading={this.state.loading} htmlType="submit">Reset Password</Button>
                    </div>
                </div>
            </Form>
        );
    }
}
const ForgetForm = Form.create()(Forget);
export default ForgetForm;