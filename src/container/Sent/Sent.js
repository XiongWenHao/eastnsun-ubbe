import React, { Component } from 'react';
import { Button } from 'antd';
import './Sent.css';
class Sent extends Component {
    constructor(props) {
        super(props);
        this.state={
            email:""
        }
        this.resend=this.resend.bind(this)
        this.done=this.done.bind(this)
    }
    //重新发送
    resend(){
      
    }
    //完成
    done(){

    }

    render() {
        const self=this.props.location.email
        this.state.email=self;
        return (
            <div className="sentBody">
                <div className="sentContainer sentBottom">
                    <div className="sentTitle sentTop sentBottom">Email sent!</div>
                    <div className="sentHint">Check your email at</div>
                    <div className="sentEmailContainer">
                        <div className="sentEmail">{this.state.email}</div>
                        <div className="sentHint sentLeft">and follow</div>
                    </div>
                    <div className="sentHint">the link to reset your password.</div>
                    <div className="sentDoubt sentTop">Didn't receive the email?<span onClick={this.resend}>Resend</span></div>
                    <Button className="sentButton sentBottom" onClick={this.done}>Done</Button>
                </div>
            </div>
        );
    }
}

export default Sent;