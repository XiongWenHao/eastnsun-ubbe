import React, { Component } from 'react';
import './Account.css';

import Header from '../../components/Header/Header';
import Content from '../../components/AccountContent/Content';
import Footer from '../../components/Footer/Footer';

class Account extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="accountBody">
                <Header />
                <Content/>
                <Footer />
            </div>
        );
    }
}

export default Account;