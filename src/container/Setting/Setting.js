import React, { Component } from 'react';
import './Setting.css';
import Header from '../../components/Header/Header';
import Content from '../../components/SettingContent/Content';
import Footer from '../../components/Footer/Footer';

class Setting extends Component {
    render() {
        return (
            <div className="settingContainer">
                <Header/>
                <Content/>
                <Footer/>
            </div>
        );
    }
}

export default Setting;