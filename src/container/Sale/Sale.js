import React, { Component } from 'react';
import './Sale.css';
import Header from '../../components/SaleHeader/SaleHeader';
import SaleContent from '../../components/SaleContent/Content2';

class Sale extends Component {
    render() {
        return (
            <div className="saleBody">
                <Header/>
                <SaleContent/>
            </div>
        );
    }
}

export default Sale;