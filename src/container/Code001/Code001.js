import React, { Component } from 'react';
import './Code001.css'

class Code001 extends Component {
    render() {
        return (
            <div className="codeContainer codeTop">
                <div className="codeTitleContainer">
                    <div className="codeLeftContainer">
                        <div className="codeLeftTitle codeTitleLeft">Code 001</div>
                        <div className="codeLeftTitle codeTitleLeft">General Sale Authority</div>
                    </div>
                    <div className="codeRightContainer">
                         <div className="codeReivContainer codeRightTitle">REIV</div> 
                         <div className="codeReivContainer">The Real Estate Institute of Victoria Ltd </div>
                         <div className="codeReivContainer">ABN 81 004 210 897</div>
                         <div className="codeReivContainer">www.reiv.com.au</div> 
                    </div>
                </div>
            </div>
        );
    }
}

export default Code001;