import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import { Form, Input, Button, Alert } from 'antd';
import './Register.css';

import axios from 'axios';

const FormItem = Form.Item;

class Register extends Component {
    constructor(props) {
        super(props);
        this.login = this.login.bind(this)
        this.state = {
            loading: false,
            visible: true,
            message: "Email address is already registered\nWould you like to sign in?",
        }
    }

    login() {
        this.props.history.push('/login')
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                this.closeProgress(true);
                const registerEmail = values.email;
                const registerPassword = values.password;
               console.log(registerEmail+"  "+registerPassword);
            }
        });
    }
    //注册
    register(email, password) {

    }

    closeProgress = (e) => {
        this.setState({
            loading: e
        });
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit}>
                <div className="registerBody">
                    <div className="registerContainer registerBottom">
                        <div className="registerTitle registerTop registerBottom">Create your account</div>
                        <div className="registerAlert">
                            {
                                this.state.visible ? (
                                    <Alert
                                        message={this.state.message}
                                        type="error"
                                        closable
                                        afterClose={this.handleClose}
                                    />
                                ) : null
                            }
                        </div>
                        <FormItem>
                            {getFieldDecorator('email', {
                                rules: [{
                                    type: 'email', message: 'The input is not valid E-mail!',
                                }, {
                                    required: true, message: 'Please input your E-mail!',
                                }],
                            })(
                                <Input size="large" style={{ width: 260 }} placeholder="Email" />
                            )}
                        </FormItem>
                        <FormItem>
                            {getFieldDecorator('password', {
                                rules: [{ required: true, message: 'Please input your Password!' }],
                            })(
                                <Input size="large" style={{ width: 260 }} type="password" placeholder="Password" />
                            )}
                        </FormItem>
                        <Button className="registerButton" loading={this.state.loading} htmlType="submit">CREATE ACCOUNT</Button>

                        <div className="registerHint registerBottom" onClick={this.login}>Already have an account?</div>
                    </div>
                </div>
            </Form>
        );
    }
}
const RegisterForm = Form.create()(Register);
export default RegisterForm;