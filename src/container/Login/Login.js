import React, { Component } from 'react';
import { Form, Input, Button, Alert } from 'antd';
import './Login.css';

import axios from 'axios';

const FormItem = Form.Item;

class Login extends Component {
    constructor(props) {
        super(props);
        this.forget = this.forget.bind(this)
        this.register = this.register.bind(this)
        this.state = {
            loading: false,
            visible: true,
            message: "Email address is already registered\nWould you like to sign in?",
        }
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                this.closeProgress(true);
                const loginEmail = values.email;
                const loginPassword = values.password;
                console.log(loginEmail + "  " + loginPassword);
            }
        });
    }
    closeProgress = (e) => {
        this.setState({
            loading: e
        });
    }
    //忘记密码
    forget() {
        this.props.history.push('/forget')
    }
    //注册
    register() {
        this.props.history.push('/register')
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit}>
                <div className="loginBody">
                    <div className="loginContainer loginBottom">
                        <div className="loginTitle loginTop loginBottom">Sign in</div>
                        <div className="loginAlert">
                            {
                                this.state.visible ? (
                                    <Alert
                                        message={this.state.message}
                                        type="error"
                                        closable
                                        afterClose={this.handleClose}
                                    />
                                ) : null
                            }
                        </div>
                        <FormItem>
                            {getFieldDecorator('email', {
                                rules: [{
                                    type: 'email', message: 'The input is not valid E-mail!',
                                }, {
                                    required: true, message: 'Please input your E-mail!',
                                }],
                            })(
                                <Input size="large" style={{ width: 260 }} placeholder="Email" />
                            )}
                        </FormItem>
                        <FormItem>
                            {getFieldDecorator('password', {
                                rules: [{ required: true, message: 'Please input your Password!' }],
                            })(
                                <Input size="large" style={{ width: 260 }} type="password" placeholder="Password" />
                            )}
                        </FormItem>
                        <Button className="loginButton" loading={this.state.loading} htmlType="submit">Sign in</Button>

                        <div className="loginHint" onClick={this.forget}>Forget password?</div>
                        <div className="loginHint loginBottom" onClick={this.register}>Dont't have an account?</div>
                    </div>
                </div>
            </Form>
        );
    }
}
const LoginForm = Form.create()(Login);
export default LoginForm;