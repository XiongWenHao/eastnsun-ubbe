export const LIST = e => [
    {
        func: e.handleSetPayload.bind(e),
        list_content: [
            {
                title: 'Email',
                type:'input',
                classContainer:'contentInputContainer contentSettingTop',
                class:'contentEmailRight'
            },
            {
                title: 'Password',
                type:'input',
                classContainer:'contentInputContainer contentSettingTop',
                class:'contentPasswordRight'
            }
        ]
    }
]