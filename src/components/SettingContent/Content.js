import React, { Component } from 'react';
import './Content.css';
import { LIST } from './data';
import Form from './Form';

class Content extends Component {
    constructor(props) {
        super(props);
        this.state = {
            step: 0,
            payload: []
        }
    }
    renderForm(self) {
        return <Form key={self.step} data={LIST(this)[self.step]} />
    }
    handleSetPayload(e) {
        this.setState({
            payload: [...this.state.payload, e],
            step: this.state.step + 1
        });
    }
    render() {
        const self = this.state;
        return (
            <div className="contentContainer">
                {this.renderForm(self)}
            </div>
        );
    }
}

export default Content;