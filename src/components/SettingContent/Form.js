import React, { Component } from 'react';
import {Input} from 'antd';

class Form extends Component {
    renderPage(e) {
        return <div className={e.classContainer}>
            <span className={e.class}>{e.title}</span>
            <Input size="large" style={{ width: 400 }} />
        </div>
    }
    renderContent(e) {
        var result = [];
        e.data.list_content.map(v => {
            result.push(this.renderPage(v));
        });
        return <div>{result}</div>
    }

    render() {
        const self = this.props
        return (
            <div>
                {this.renderContent(self)}
            </div>
        );
    }
}

export default Form;