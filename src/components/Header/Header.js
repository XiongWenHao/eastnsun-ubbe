import React, { Component } from 'react';
import './Header.css';

class Header extends Component {
    render() {
        return (
            <div className="headerContainer">
                 <h2 className="header">Account</h2>
            </div>
        );
    }
}

export default Header;