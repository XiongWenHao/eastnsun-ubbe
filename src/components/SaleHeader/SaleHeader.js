import React, { Component } from 'react';
import { Input } from 'antd';
import './SaleHeader.css';

const Search = Input.Search;

class Header2 extends Component {
    render() {
        return (
            <div className="headerContainer2">
                <h2 className="headerLeft headerRight">Property / Sale</h2>
                <Search
                    placeholder="input search text"
                    enterButton="Search"
                    size="large"
                    style={{width:600}}
                    onSearch={value => console.log(value)}
                />
            </div>
        );
    }
}

export default Header2;