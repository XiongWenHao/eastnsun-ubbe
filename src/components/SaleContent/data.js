export const LIST = e => [
    {
        func: e.handleSetPayload.bind(e),
        list_content: [
            {
                leftImg: '',
                leftTitleTop: '172-176 Old Dandenong Road',
                leftTitleBottom: 'Heatherton Vic 3202',
                rightTitleGeneral: 'General Features',
                rightType: '7',
                rightBedrooms: '7',
                rightBathrooms: '3',
                rightLandSize: '0.82ha(2.01 acres)(approx)',
                rightOutdoor: 'Outdoor Features',
                rightSpace: 'Garage Space:2',
                type: 'info'
            },
            {
                leftImg: '',
                leftTitleTop: '172-176 Old Dandenong Road',
                leftTitleBottom: 'Heatherton Vic 3202',
                rightTitleGeneral: 'General Features',
                rightType: '7',
                rightBedrooms: '7',
                rightBathrooms: '3',
                rightLandSize: '0.82ha(2.01 acres)(approx)',
                rightOutdoor: 'Outdoor Features',
                rightSpace: '2',
                type: 'info'
            }
        ]
    },
    {
        func: e.handleSetPayload.bind(e),
        list_content: [
            {
                title: 'U22 Catalina Ave Ashburton',
                btnTitle: 'Action',
                type: 'all'
            },
            {
                title: 'U22 Catalina Ave Ashburton',
                btnTitle: 'Action',
                type: 'all'
            },
            {
                title: 'U22 Catalina Ave Ashburton',
                btnTitle: 'Action',
                type: 'all'
            },
            {
                title: 'U22 Catalina Ave Ashburton',
                btnTitle: 'Action',
                type: 'all'
            },
        ]
    },
    {
        func: e.handleSetPayload.bind(e),
        list_content: [
            {
                topTitle: 'Statement of information',
                bottomTitle: 'General sale authority',
                btnTitle: 'Action',
                type: 'progress'
            },
            {
                topTitle: 'Statement of information',
                bottomTitle: 'Exclusive sale authority',
                btnTitle: 'Action',
                type: 'progress'
            },
            {
                topTitle: 'Statement of information',
                bottomTitle: 'Exclusive auction authority',
                btnTitle: 'Action',
                type: 'progress'
            },
            {
                topTitle: 'Contract of Sale of real estate',
                btnTitle: 'Action',
                type: 'progress2'
            },
            {
                topTitle: 'Contract of Sale of real estate',
                btnTitle: 'Action',
                type: 'progress2'
            },
        ]
    },
]