import React, { Component } from 'react';
import { Tabs, Button, Icon, Input, Table } from 'antd';
import './Content.css';
const TabPane = Tabs.TabPane;

const columns = [{
    title: 'Address of comparable property',
    dataIndex: 'address of comparable property',
    render: text => <a href="javascript:;">{text}</a>,
}, {
    title: 'Price',
    className: 'price',
    dataIndex: 'money',
}, {
    title: 'Date of sale',
    dataIndex: 'date of sale',
}];

function callback(key) {
    console.log(key);
}

class Content2 extends Component {
    renderAll() {
        return <div className="contentAllContainer">
            <div>U22 Catalina Ave Ashburton</div>
            <Button>Action</Button>
        </div>
    }
    renderProcessing() {
        return <div className="ContentProgressContainer" >
            <div className="contentChooseContainer contentLeft contentTop">
                <div className="contentSoiContainer">
                    <div className="contentLeft">Statement of information</div>
                    <Button>Action</Button>
                </div>
                <div className="contentSoiContainer contentTop">
                    <div className="contentLeft">General sale authority</div>
                    <Button>Action</Button>
                </div>
            </div>
            <div className="contentChooseContainer contentLeft contentTop">
                <div className="contentSoiContainer">
                    <div className="contentLeft">Statement of information</div>
                    <Button>Action</Button>
                </div>
                <div className="contentSoiContainer contentTop">
                    <div className="contentLeft">General sale authority</div>
                    <Button>Action</Button>
                </div>
            </div>
            <div className="contentChooseContainer contentLeft contentTop">
                <div className="contentSoiContainer">
                    <div className="contentLeft">Statement of information</div>
                    <Button>Action</Button>
                </div>
                <div className="contentSoiContainer contentTop">
                    <div className="contentLeft">General sale authority</div>
                    <Button>Action</Button>
                </div>
            </div>
            <div className="contentChooseContainer contentTop contentBottom contentLeft">
                <div className="contentSoiContainer">
                    <div className="contentLeft">Contract of Sale of real estate</div>
                    <Button>Action</Button>
                </div>
            </div>
            <div className="contentChooseContainer contentTop contentBottom contentLeft">
                <div className="contentSoiContainer">
                    <div className="contentLeft">Contract of Sale of real estate</div>
                    <Button>Action</Button>
                </div>
            </div>
        </div>
    }

    renderProcessing2() {
        return <div className="contentSioTableContainer">
            <div className="contentSioTitle contentSioBold">Statement of Information</div>
            <div className="contentSioTitle">Internet advertising for single residential property located within or outside the</div>
            <div className="contentSioTitle">Melbourne metropolitan area</div>
            <div className="contentSioTableTitleContainer">Sections 47AF of the Estate Agents Act 1980</div>
            <div className="contentSioTable">
                <div className="contentSioTableText contentSioLeft contentPriceTop"><span>Instructions:</span> The instructions in this box do not form part of this Statement of Information and are not required to be included in an internet advertisement.</div>
                <div className="contentSioTableText contentSioLeft">The Director of Consumer Affairs Victoria has approved this form of the Statement of Information for section 47AF of the Estate Agents Act 1980.</div>
                <div className="contentSioTableText contentSioLeft">The estate agent or agent’s representative engaged to sell the property is required to prepare this Statement of information. It must be included <span>with any advertisement for the sale of a single residential property</span> published by or on behalf of an estate agent or agent’s representative on any Internet site during the period that the residential property is offered for sale.</div>
                <div className="contentSioTableText contentSioLeft">The indicative selling price may be expressed as a single price, or as a price range with the difference between the upper and lower amounts not more than 10% of the lower amount. </div>
                <div className="contentSioTableText contentSioLeft">If the property for sale is in the Melbourne metropolitan area, a comparable property must be within two kilometres and have sold within the last six months. If the property for sale is outside the Melbourne metropolitan area, a comparable property must be within five kilometres and have sold within the last 18 months. The Determination setting out the local government areas that comprise the Melbourne metropolitan area is published on the Consumer Affairs Victoria website at <span>consumer.vic.gov.au/underquoting.</span></div>
                <div className="contentSioTableText contentSioLeft">It is recommended that the address of the property being offered for sale be checked at services.land.vic.gov.au/landchannel/content/addressSearch.</div>
            </div>
            <div className="contentSioPrice contentTop">Indicative selling price <Icon type="info-circle-o" /><span> For the meaning of this price see consumer.vic.gov.au/underquoting</span></div>
            <div className="contentSioTableText contentSioTop">(*Delete single price or range as applicable)</div>
            <div className="contentSioTableTitleContainer contentSioTop">Single price <Input className="contentSioLeft contentSioRight" style={{ width: 200 }} />  or range between <Input className="contentSioRight contentSioLeft" style={{ width: 200 }} /> & <Input className="contentSioLeft" style={{ width: 200 }} /></div>
            <div className="contentSioPrice contentTop">Median sale price</div>
            <div className="contentSioTableText contentSioTop">(*Delete house or unit as applicable)</div>
            <div className="contentSioPrice contentSioTop">Median price <Input className="contentSioLeft contentRight2" style={{ width: 200 }} /> *House<Input className="contentSioLeft contentRight2" style={{ width: 100 }} />*unit<Input className="contentSioLeft contentSioRight2" style={{ width: 100 }} />Suburb or locality<Input className="contentSioLeft" style={{ width: 200 }} /></div>
            <div className="contentSioTableTitleContainer contentSioTop">Period-Form<Input className="contentSioLeft contentSioRight" style={{ width: 200 }} /> to <Input className="contentSioRight contentSioLeft" style={{ width: 200 }} />Source<Input className="contentSioLeft" style={{ width: 280 }} /></div>
            <div className="contentSioPrice contentTop">Comparable property sales (*Delete A or B below as applicable)</div>
            <div className="contentSioTableTextContainer contentSioTop">
                <div className="contentSioPrice">A*</div>
                <div className="contentAContainer">
                    <div className="contentSioTableText">These are the three properties sold within two kilometres/five kilometres* of the property for sale in the last six</div>
                    <div className="contentSioTableText">months/18 months* that the estate agent or agent’s representative considers to be most comparable to the </div>
                    <div className="contentSioTableText">property for sale. (*Delete as applicable)</div>
                </div>
            </div>
            <div className="contentSioTableTextContainer contentSioTop">
                <div className="contentSioTableText">Address of comparable property</div>
                <div className="contentSioTableText">Price</div>
                <div className="contentSioTableText">Date of sale</div>
            </div>
            <div className="contentSioTableTextContainer contentSioTop">
                <div><Input size="large" style={{ width: 500 }} /></div>
                <div><Input prefix={<i class="iconfont">&#xe67b; </i>} size="large" style={{ width: 290 }} /></div>
                <div><Input size="large" style={{ width: 290 }} /></div>
            </div>
            <div className="contentSioTableTextContainer">
                <div><Input size="large" style={{ width: 500 }} /></div>
                <div><Input prefix={<i class="iconfont">&#xe67b; </i>} size="large" style={{ width: 290 }} /></div>
                <div><Input size="large" style={{ width: 290 }} /></div>
            </div>
            <div className="contentSioTableTextContainer">
                <div><Input size="large" style={{ width: 500 }} /></div>
                <div><Input prefix={<i class="iconfont">&#xe67b; </i>} size="large" style={{ width: 290 }} /></div>
                <div><Input size="large" style={{ width: 290 }} /></div>
            </div>
            <div className="contentSioPrice contentSioTop">OR</div>
            <div className="contentSioTableContainer2">
                <div className="contentSioPrice contentSioTop">B</div>
                <div className="contentSioEnterContainer">
                    <div className="contentSioPrice contentSioTop contentSioLeft2 contentSioRight3">Enter</div>
                    <div className="contentSioPrice contentSioLeft2 contentSioTop2">Or</div>
                </div>
                <div className="contentAContainer contentSioTop" >
                    <div className="contentSioTableText">The estate agent or agent’s representative reasonably believes that fewer than three comparable </div>
                    <div className="contentSioTableText">properties were sold within two kilometres of the property for sale in the last six months* </div>
                    <div className="contentSioTableText">The estate agent or agent’s representative reasonably believes that fewer than three comparable </div>
                    <div className="contentSioTableText">properties were sold within five kilometres of the property for sale in the last 18 months*. </div>
                </div>
            </div>
            <div className="contentSioTableText contentSioLeft2 contentSioTop">(*Delete as applicable)</div>
            <div className="contentSioBtnContainer contentBtnTop contentBtnBottom">
                <Button className="contentSioBtn contentBtnRight contentBtnCancel">Cancel</Button>
                <Button className="contentSioBtn contentBtnSave">Save</Button>
            </div>
        </div>
    }

    renderCompleted() {
        return <div className="contentAllContainer">
            <div>U22 Catalina Ave Ashburton</div>
            <div>
                <Button>Action</Button>
                <span>SOLD</span>
            </div>

        </div>
    }

    render() {
        return (
            <div className="contentContainer">
                <Tabs size="large" defaultActiveKey="2" onChange={callback}>
                    <TabPane tab="ALL" key="1">{this.renderAll()}</TabPane>
                    <TabPane tab="Processing" key="2">{this.renderProcessing2()}</TabPane>
                    <TabPane tab="Completed" key="3">{this.renderCompleted()}</TabPane>
                </Tabs>
            </div>
        );
    }
}

export default Content2;