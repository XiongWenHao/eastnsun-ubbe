import React, { Component } from 'react';
import './Content.css';
import { Button } from 'antd';
import { LIST } from './data';
import SaleContent from './SaleContent';

class Content extends Component {
    constructor(props) {
        super(props);
        this.state = {
            step: 0,
            payload: []
        }
    }
    renderContent(self) {
        return <SaleContent key={self.step} data={LIST(this)[self.step]} />
    }
    handleSetPayload(e) {
        this.setState({
            payload: [...this.state.payload, e],
            step: this.state.step + 1
        });
    }
    render() {
        const self = this.state;
        return (
            <div className="contentContainer">
                {this.renderContent(self)}
               
            </div>
        );
    }
}

export default Content;