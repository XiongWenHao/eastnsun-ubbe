import React, { Component } from 'react';
import { Button, Tabs } from 'antd';
import Footer from '../SaleFooter/SaleFooter';
const TabPane = Tabs.TabPane;

class SaleContent extends Component {

    renderPage(e) {
        switch (e.type) {
            case "info":
                return <div className="contentInfoContainer">
                    <div className="contentLeftContainer contentRight">
                        <div className="contentImg">
                        </div>
                        <div className="contentDescription">
                            <div className="contentAddress contentLeft">{e.leftTitleTop}</div>
                            <div className="contentName contentLeft">{e.leftTitleBottom}</div>
                        </div>
                    </div>
                    <div className="contentRightContainer">
                        <h1>{e.rightTitleGeneral}</h1>
                        <div className="contentType">{e.rightType}</div>
                        <div className="contentType">{e.rightBedrooms}</div>
                        <div className="contentType">{e.rightBathrooms}</div>
                        <div className="contentType">{e.rightLandSize}</div>
                        <h1 className="contentTop">{e.rightOutdoor}</h1>
                        <div className="contentType">{e.rightSpace}</div>
                        <div className="contentTop"><Button type="primary" onClick={() => this.props.data.func(e)}>Confirm</Button></div>
                    </div>
                </div>
                break;
            case "all":
                return <div className="contentAllContainer">
                    <div>{e.title}</div>
                    <Button onClick={() => this.props.data.func(e)}>{e.btnTitle}</Button>
                </div>
                break;
            case "progress":
                return <div>
                    <div className="contentChooseContainer contentLeft contentTop">
                        <div className="contentSoiContainer">
                            <div className="contentLeft">{e.topTitle}</div>
                            <Button>{e.btnTitle}</Button>
                        </div>
                        <div className="contentSoiContainer contentTop">
                            <div className="contentLeft">{e.bottomTitle}</div>
                            <Button>{e.btnTitle}</Button>
                        </div>
                    </div>
                </div>
                break;
            case "progress2":
                return <div>
                    <div className="contentChooseContainer contentTop contentBottom contentLeft">
                        <div className="ContentSoiContainer">
                            <div className="contentLeft">{e.topTitle}</div>
                            <Button>{e.btnTitle}</Button>
                        </div>
                    </div>
                </div>
                break;
        }

    }
    renderContent(e) {
        var result = [];
        e.data.list_content.map(v => {
            result.push(this.renderPage(v));
        });
        if (e.data.list_content[0].type === "progress" || e.data.list_content[0].type === "progress2") {
            return <Tabs defaultActiveKey="1">
                <TabPane tab="ALL" key="2"></TabPane>
                <TabPane tab="Processing" key="2"><div className="ContentProgressContainer">{result}</div></TabPane>
                <TabPane tab="Completed" key="3"></TabPane>
            </Tabs>

        } else if (e.data.list_content[0].type === "all") {
            return <Tabs defaultActiveKey="1">
                <TabPane tab="ALL" key="1"><div>{result}
                    <Footer />
                </div></TabPane>
                <TabPane tab="Processing" key="2"></TabPane>
                <TabPane tab="Completed" key="3"></TabPane>
            </Tabs>

        } else {
            return <div>{result}
                <Footer />
            </div>
        }

    }
    render() {
        const self = this.props
        return (
            <div>
                {this.renderContent(self)}
            </div>
        );
    }
}

export default SaleContent;