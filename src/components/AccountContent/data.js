export const HEADER = [
    {
        title: 'Choose your identity'
    },
    {
        title: 'Choose your profession'
    }
];

export const LIST = e => [
    {
        func: e.handleSetPayload.bind(e),
        list_content: [
            {
                img: '../../assets/img/indentity/person.png',
                title: 'Person',
                btnTitle: 'Enter',
                type:'identity'
            },
            {
                img: '../../assets/img/indentity/company.png',
                title: 'Company',
                btnTitle: 'Enter',
                type:'identity'
            }
        ]
    },
    {
        func: e.handleSetPayload.bind(e),
        list_content: [
            {
                title: 'Agency',
                type:'profession'
            },
            {
                title: 'Lawyer',
                type:'profession'
            },
            {
                title: 'Barrister',
                type:'profession'
            },
            {
                title: 'Broker',
                type:'profession'
            }
        ]
    }
]