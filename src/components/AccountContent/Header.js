import React, { Component } from 'react';

class Header extends Component {
    renderTitle(e){
        return <h1 className="contentTitle">{e.title}</h1>
    }
    render() {
        const self =this.props;
        return (
            <div>
                {this.renderTitle(self)}
            </div>
        );
    }
}

export default Header;