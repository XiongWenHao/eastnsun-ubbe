import React, { Component } from 'react';
import './Content.css';
import Person from '../../assets/img/indentity/person.png';
import Company from '../../assets/img/indentity/company.png';
import Header from './Header';
import Choose from './Choose';
import { Button } from 'antd';
import { HEADER, LIST } from './data';

class Content extends Component {

    constructor(props) {
        super(props);
        this.state = {
            step: 0,
            payload:[]
        }
    }

    renderHeader(self) {
        return HEADER.map((v, index) => {
            if (self.step == index) {
                return (
                    <Header
                        key={index}
                        title={v.title}
                    />
                );
            }
        });
    }

    renderContent(self) {
        return <Choose key={self.step} data={LIST(this)[self.step]} />
    }

    handleSetPayload(e) {
        this.setState({
            payload: [...this.state.payload, e],
            step: this.state.step + 1
        });
    }

    render() {
        const self = this.state;
        return (
            <div className="contentContainer">
                {this.renderHeader(self)}
                {this.renderContent(self)}
            </div>
        );
    }
}

export default Content;