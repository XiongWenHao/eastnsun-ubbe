import React, { Component } from 'react';
import { Button } from 'antd';

class Choose extends Component {

    renderPage(e) {
        console.log(e);
        switch (e.type) {
            case "identity":
                return <div className="contentChooseBgContainer">
                    <img src={e.img} />
                    <span className="contentPerson contentAccountTop">{e.title}</span>
                    <Button className="contentButton contentAccountTop" onClick={() => this.props.data.func(e)}>{e.btnTitle}</Button>
                </div>
                break;
            case "profession":
                return <div className="contentIdentityContainer">
                    <div className="contentProfession">
                        {e.title}
                    </div>
                </div>
                break;
        }
    }

    renderContent(e) {
        var result = [];
        e.data.list_content.map(v => {
            result.push(this.renderPage(v));
        });
        if (e.data.list_content[0].type === "identity") {
            return <div className="contentChooseContainer">{result}</div>
        } else {
            return <div className="contentChooseContainer contentChooseWrap">{result}</div>
        }

    }
    render() {
        const self = this.props
        return (
            <div>
                {this.renderContent(self)}
            </div>
        );
    }
}

export default Choose;