import React, { Component } from 'react';
import { Pagination } from 'antd';

class SaleFooter extends Component {
    render() {
        return (
            <div className="footerContainer">
                <Pagination defaultCurrent={6} total={500} />
            </div>
        );
    }
}

export default SaleFooter;