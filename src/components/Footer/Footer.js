import React, { Component } from 'react';
import Logo from '../../assets/img/logo/logo.png';
import './Footer.css';

class Footer extends Component {
    render() {
        return (
            <div className="footerContainer">
                 <img src={Logo}/>
                 <div className="footerDescription footerTop">Copyright © 2018Eastnsun,All Rights Resered.  Privacy  Terms</div>
            </div>
        );
    }
}

export default Footer;