import React from 'react';
import './App.css';
import 'antd/dist/antd.css';
import { BrowserRouter, Route } from 'react-router-dom';

import Register from './container/Register/Register';
import Login from './container/Login/Login';
import Forget from './container/Forget/Forget';
import Sent from './container/Sent/Sent';
import Account from './container/Account/Account';
import Setting from './container/Setting/Setting';
import Sale from './container/Sale/Sale';
import Code001 from './container/Code001/Code001';

const App = () => (
  <BrowserRouter>
    <div>
      <Route exact path='/register' component={Register} />
      <Route exact path='/login' component={Login} />
      <Route exact path='/forget' component={Forget} />
      <Route exact path='/sent' component={Sent} />
      <Route exact path='/account' component={Account} />
      <Route exact path='/setting' component={Setting} />
      <Route exact path='/sale' component={Sale} />
      <Route exact path='/code001' component={Code001} />
    </div>
  </BrowserRouter>
);

export default App;
